var express = require('express');
var router = express.Router();
var redis = require('redis'),
    redisClient = redis.createClient();
var userNamesAndTweets = [];
var publishingAccounts = "twitter_accounts";

var getTweets = function (req, res, next) {
  userNamesAndTweets = []
  redisClient.smembers(publishingAccounts, function (error, accounts) {
    if (error) {
      logAndThrowError(error)
    }

    accounts.forEach(function (account, accountIndex) {
      counterName = getCounterName(account);
      redisClient.get(counterName, function (error, tweetsAmount) {

        if (error) {
          logAndThrowError(error)
        }

        // На данный момент у нас есть значение счётчика.
        // То есть я могу распечатать все интересующие меня твиты от
        // нулевого значения до значения счётчика, включая его.
        for (var tweetNumber = 1; tweetNumber <= tweetsAmount; tweetNumber++) {
          var hashName = getTweetHashName(account, tweetNumber);
          (function (hashName, lastAccount, lastTweet) {
            redisClient.hget(hashName, "userName", function (error, userName) {
              redisClient.hget(hashName, "text", function (error, text) {
                userNamesAndTweets.push({ userName: userName, text: text});
                if (lastAccount && lastTweet) {
                  res.render('tweets', { title: 'Твиты', tweets: userNamesAndTweets });
                }
              });
            });
          })(hashName, accountIndex == accounts.length - 1, tweetNumber == tweetsAmount);
        }
      });
    });
  });
};

function getCounterName (account) {
  return "counter:" + account;
}

function getTweetHashName (account, tweetNumber) {
  return account + ":" + tweetNumber;
}

function logAndThrowError (error) {
  console.log(error);
  throw error;
}

/* GET home page. */
router.get('/', getTweets);

module.exports = router;