var express = require('express');
var router = express.Router();
var redis = require('redis'),
    redisClient = redis.createClient();
var publishingAccounts = "twitter_accounts";
var cheerio = require("cheerio");
var request = require("request");
var tdh = require("../node_modules/terbiy-data-helpers/terbiy-data-helpers");
var responses = [];

var getPageInfo = function (req, res, next) {
  responses = [];
  redisClient.smembers(publishingAccounts, function (error, accounts) {
    if (error) {
      logAndThrowError(error)
    }

    accounts.forEach(function (account, accountIndex) {
      counterName = tdh.getCounterName(account);
      redisClient.get(counterName, function (error, tweetsAmount) {

        if (error) {
          logAndThrowError(error)
        }

        // На данный момент у нас есть значение счётчика.
        // То есть я могу распечатать все интересующие меня твиты от
        // нулевого значения до значения счётчика, включая его.
        for (var tweetNumber = 1; tweetNumber <= tweetsAmount; tweetNumber++) {
          var hashName = tdh.getTweetHashName(account, tweetNumber);
          (function (hashName, lastAccount, lastTweet) {
            redisClient.hget(hashName, "entities", function (error, entitiesString) {
              var entities = JSON.parse(entitiesString);
              var urls = entities.urls;
              for (var urlIndex = 0; urlIndex < urls.length; urlIndex++) {
                var url = urls[urlIndex].url;

                request(url, function (error, response, html) {
                  var $ = cheerio.load(html),
                      interestingElements = $("[class*='job']").get(),
                      domElement,
                      text;
                  
                  for (var i = 0; i < interestingElements.length; i++) {
                    domElement = interestingElements[i];
                    text = $(domElement).text();
                    responses.push(text);
                  }

                  if (lastAccount && lastTweet) {
                    res.render('linkedPages', { title: 'Ответы страниц', responses: responses });
                  }
                });
              }
            });
          })(hashName, accountIndex == accounts.length - 1, tweetNumber == tweetsAmount);
        }
      });
    });
  });
};

function logAndThrowError (error) {
  console.log(error);
  throw error;
}

/* GET home page. */
router.get('/', getPageInfo);

module.exports = router;