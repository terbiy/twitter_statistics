var app = require('./app.js');
var Twitter = require('twitter');
var port = process.env.PORT || 1337;
var redis = require('redis'),
    redisClient = redis.createClient();
var fileSystem = require("fs");
var tdh = require("./node_modules/terbiy-data-helpers/terbiy-data-helpers");
var remoteListIDs = [];
var interest;
var termsArray;
var publishingAccounts = tdh.getPublishingAccountsKey();
var request = require("request");
var cheerio = require("cheerio");
var escapeStringRegexp = require('escape-string-regexp');


var client = new Twitter({
  consumer_key: 'tJUJO80gWagIvRnxaDF3Dft0C',
  consumer_secret: 'yyoGPP4Pcbt6vtP41sUjTBg9caDKYrK8798yjMVQsfx5jEXCSh',
  access_token_key: '219059640-QaK78mH6kay3hvE54RFkL4x8jzHyDyjrhSdOoZpx',
  access_token_secret: 'tGfAqKmBHR4SY8O7aaesL1jrHnO6RRBpt2qMe0VwI3svN'
});


fileSystem.readFile("./terms/termsList.txt", "utf-8", function(error, terms) {
  var termsCountersNames = [], term;
  termsArray = terms.split(",");

  // На основании названий технологий создаём имена счётчиков.
  for (var termIndex = 0; termIndex < termsArray.length; termIndex++) {
    term = termsArray[termIndex];
    termsCountersNames.push(tdh.getTermCounterName(term))
  }

  // При запуске сервера я хочу пробежаться по всем отслеживаемым терминам и проверить,
  // существует ли на данный момент в базе счётчик вида "term_counter:<название термина>".
  redisClient.mget(termsCountersNames, function(error, values) {
    var termCounterName, value, countersToSet = [];

    // Остаётся лишь один небольшой вопрос — в том же ли порядке выводятся
    // значения, как и запрашиваются?
    // В оригинальном MGET на сайте Redis это так.
    for (var counterNameIndex = 0; counterNameIndex < termsCountersNames.length; counterNameIndex++) {
      termCounterName = termsCountersNames[counterNameIndex];
      value = values[counterNameIndex];
      // Если такого счётчика нет, создать его и инициализировать 0.
      if (value == null) {
        console.log(termCounterName + ": " + value);
        countersToSet.push(termCounterName, 0);
      }
    }

    if (countersToSet.length > 0) {
      redisClient.mset(countersToSet, redis.print);
    }

    startTweetAnalysis();
  });
});

function startTweetAnalysis () {
  fileSystem.readFile("./terms/interest.txt", "utf-8", function(error, response) {
    console.log("interests read");
    interest = response.split(",");

    client.get('lists/members', { slug: 'remote', owner_screen_name: 'terbiyarchitect', count: 100 }, function(error, members, response) {
      if (error) {
        logAndThrowError(error);
      }

      for (var memberCount = 0, membersLength = members.users.length; memberCount < membersLength; memberCount++) {
        var user = members.users[memberCount],
            userId = user.id;
        remoteListIDs.push(userId);
      }

      // Set streaming for the list.
      // Данный обработчик зависит как от подключения базы, так и от готовности листа.
      // Нужно проработать данный момент при помощи async.
      client.stream('statuses/filter', { follow: remoteListIDs.join() }, function(stream) {
        console.log("stream set");

        stream.on('data', function(tweet) {
          // В том случае, если в тексте твита встречаются ключевые слова
          if (stringContainsSomeWords(tweet.text, interest)) {

            var counterName = tdh.getCounterName(tweet.user.name);
            // Вносим в набор пишущих аккаунтов имя написавшего твит.
            redisClient.sadd(publishingAccounts, tweet.user.name);
            // Увеличиваем на 1 счётчик количества твитов от данного аккаунта.
            redisClient.incr(counterName);
            // Получаем текущее значение счётчика.
            redisClient.get(counterName, function(error, counter) {

              if (error) {
                logAndThrowError(error);
              }

              var hashName = tdh.getTweetHashName(tweet.user.name, counter);

              // Создаём хэш в формате <имя пользователя>:<номер счётчика>.
              // Вносим в качестве полей имя пользователя, текст твита,
              // дату создания, дополнительную информацию.
              redisClient.hset(hashName, "userName", tweet.user.name);
              redisClient.hset(hashName, "text", tweet.text);
              redisClient.hset(hashName, "created_at", tweet.created_at);
              redisClient.hset(hashName, "entities", JSON.stringify(tweet.entities));

              // Получить страницы по ссылкам, указанным в твите.
              var urls = tweet.entities.urls;

              urls.forEach(function(urlObject, urlIndex) {
                var url = urlObject.url;

                request(url, function(error, response, html) {
                  // Попытаться найти элементы страниц, классы которых содержат "job".
                  var $ = cheerio.load(html),
                      interestingElements = $("[class*='job']");
                  
                  // Если таких нет,
                  //  Использовать body.
                  if (interestingElements.length == 0) {
                    interestingElements = $("body");
                  }

                  interestingElements.each(function(elementIndex, element) {
                    // Получить текстовое содержимое элементов.
                    var text = $(element).text().toLocaleLowerCase(),
                        term,
                        escapedTerm,
                        termCounterName,
                        termOccurrences;
                    // Произвести поиск для каждого термина.
                    for (var termIndex = 0; termIndex < termsArray.length; termIndex++) {
                      term = termsArray[termIndex];
                      escapedTerm = escapeStringRegexp(term);
                      termRegExp = new RegExp(escapedTerm, "g");
                      termOccurrences = (text.match(termRegExp) || []).length;
                      // Если термин встречается в анализируемом тексте,
                      if (termOccurrences > 0) {
                      //  Увеличить значение счётчика "term_counter:<название термина>" на то количество раз, сколько он встречается.
                        termCounterName = tdh.getTermCounterName(term);
                        console.log(termCounterName + ": " + termOccurrences);
                        redisClient.incrby(termCounterName, termOccurrences);
                      }
                    }
                  });
                });
              });
            });
          }
        });

        stream.on('error', logAndThrowError);
      });
    });
  });
}

// var userNamesAndTweets = [];
// redisClient.on("ready", function() {
//   // По готовности базы произвести печать всех сохранённых твитов.
//   redisClient.smembers(publishingAccounts, function(error, accounts) {
//     if (error) {
//       logAndThrowError(error)
//     }

//     accounts.forEach(function(account) {
//       counterName = tdh.getCounterName(account);
//       redisClient.get(counterName, function(error, tweetsAmount) {

//         if (error) {
//           logAndThrowError(error)
//         }

//         // На данный момент у нас есть значение счётчика.
//         // То есть я могу распечатать все интересующие меня твиты от
//         // нулевого значения до значения счётчика, включая его.
//         for (var tweetNumber = 1; tweetNumber <= tweetsAmount; tweetNumber++) {
//           var hashName = tdh.getTweetHashName(account, tweetNumber);
//           (function(hashName) {
//             redisClient.hget(hashName, "userName", function(error, userName) {
//               redisClient.hget(hashName, "text", function(error, text) {
//                 userNamesAndTweets.push({ userName: userName, text: text});
//                 console.log(userName + ": " + text);
//               });
//             });
//           })(hashName);
//         }
//       });
//     });
//   });
// });


redisClient.on("error", logAndThrowError);

app.listen(port);

function stringContainsSomeWords (string, words) {
  for (var index = 0; index < words.length; index++) {
    lowerCaseString = string.toLocaleLowerCase();
    word = words[index].toLocaleLowerCase();
    if (lowerCaseString.indexOf(word) >= 0) return true;
  }
  return false;
}

function logAndThrowError (error) {
  console.log(error);
  throw error;
}